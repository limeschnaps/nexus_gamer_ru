#!/bin/bash

echo -e "Reinitialising local DB..."
echo -e "Dropping 'moneo_shop' tables..."
python manage.py sqlclear moneo_shop | python manage.py dbshell
echo -e "Syncing local DB..."
python manage.py syncdb --noinput
echo -e "Loading fixtures..."
python manage.py loaddata moneo_shop/fixtures/initial/shop_categories.json
python manage.py loaddata moneo_shop/fixtures/initial/shop_gameshopitems.json
python manage.py loaddata moneo_shop/fixtures/initial/shop_specials.json
# echo -e "Creating superuser..."
# python manage.py createsuperuser --user=lime --email=akulov@adwz.ru
echo -e "Finished!"

