from django.conf.urls import patterns, include, url

from django.contrib import admin
from django.conf import settings
import tinymce
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    url(r'^$', 'moneo_shop.views.home', name='home'),
    url(r'^faq/$', 'moneo_shop.views.faq', name='faq'),
    url(r'^feedback/$', 'moneo_shop.views.feedback', name='feedback'),
    url(r'^cart/action/check-payment/$', 'moneo_shop.views.cart_check_payment_action', name='cart-check-payment-action'),

    url(r'^cart/$', 'moneo_shop.views.cart', name='cart'),
    url(r'^cart/submit/$', 'moneo_shop.views.cart_submit', name='cart-submit'),
    url(r'^cart/payment/$', 'moneo_shop.views.cart_payment', name='cart-payment'),
    # url(r'^cart/action/check-payment/$', 'moneo_shop.views.cart_check_payment_action', name='cart-check-payment-action'),

    url(r'^cart/action/add/$', 'moneo_shop.views.cart_add_action', name='cart-add-action'),
    url(r'^cart/action/remove/$', 'moneo_shop.views.cart_remove_action', name='cart-remove-action'),
    url(r'^goods/(?P<pk>[\d]+)/$', 'moneo_shop.views.details', name='details'),

    url(r'^goods/genre/(?P<genre>[-\w]{0,})/$', 'moneo_shop.views.genre', name='genre'),
    url(r'^goods/(?P<category>[-\w]{0,})/$', 'moneo_shop.views.category', name='category'),
    url(r'^goods/(?P<category>[-\w]{0,})/(?P<subcategory>[-\w]{0,})/$', 'moneo_shop.views.category', name='category'),
    url(r'^wow/gold/$', 'moneo_shop.views.gold_trade', name='gold-trade'),
    url(r'^search/$', 'moneo_shop.views.search', name='search'),

    # url(r'^moneo/', include('moneo.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
)

urlpatterns += patterns('',
    url(r'^tinymce/', include('tinymce.urls')),
)

# Supercaptcha URL
urlpatterns += patterns('',
    url(r'^captcha/(?P<code>[\da-f]{32})/$', 'supercaptcha.draw')
)

urlpatterns += patterns('',
        url(r'^media/(?P<path>.*)$',
            'django.views.static.serve',
            {'document_root': settings.MEDIA_ROOT, }),
        url(r'^static/(?P<path>.*)$',
            'django.views.static.serve',
            {'document_root': settings.STATIC_ROOT, }),
    )