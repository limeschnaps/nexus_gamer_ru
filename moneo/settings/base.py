import os
from django.core.exceptions import ImproperlyConfigured

try:
    VIRTUAL_ENV = os.environ['VIRTUAL_ENV']
except:
    raise ImproperlyConfigured('This should be used inside venv')

# Django settings for moneo project.

DEBUG = True
TEMPLATE_DEBUG = DEBUG

ADMINS = (
    # ('Your Name', 'your_email@example.com'),
)

MANAGERS = ADMINS

TIME_ZONE = 'Europe/Moscow'

LANGUAGE_CODE = 'ru-ru'

SITE_ID = 1

USE_I18N = True
USE_L10N = True
USE_TZ = True

MEDIA_ROOT = os.path.join(VIRTUAL_ENV, 'www/media')
MEDIA_URL = '/media/'

STATIC_ROOT = os.path.join(VIRTUAL_ENV, 'www/static')
STATIC_URL = '/static/'

STATICFILES_DIRS = (
)

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    # 'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

SECRET_KEY = 'ahqb)f*_8@$4t)tza)@f%g5au!$pc9baenccikhsdvq-aq4j)%'

TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
    # 'django.template.loaders.eggs.Loader',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    # 'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'moneo.urls'

WSGI_APPLICATION = 'moneo.wsgi.application'

TEMPLATE_DIRS = (
)

# TINYMCE_SPELLCHECKER = True
# TINYMCE_COMPRESSOR = True
TINYMCE_DEFAULT_CONFIG = {
    # 'plugins': "table,spellchecker,paste,searchreplace,link,unlink",
    # 'theme': "advanced",
    # 'cleanup_on_startup': True,
    # 'custom_undo_redo_levels': 10,
}

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    # 'django_admin_bootstrapped',
    'django.contrib.admin',
    'sorl.thumbnail',
    'supercaptcha',
    'tinymce',
    # 'django.contrib.admindocs',
    'moneo_shop'
)

CAPTCHA_LENGTH = 4
CAPTCHA_SIZE = (100, 41)

ROBOKASSA_SHOP_NAME = 'Nexus Gamer'
ROBOKASSA_MERCHANT_LOGIN = 'demo'
ROBOKASSA_MERCHANT_PASS_1 = 'password_1'
ROBOKASSA_MERCHANT_PASS_2 = 'password_2'
ROBOKASSA_CULTURE = 'ru'
ROBOKASSA_ENCODING = 'utf-8'

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}
