# -*- coding: utf-8 -*-

__author__ = 'limeschnaps'

from .base import *

DEBUG = False
TEMPLATE_DEBUG = DEBUG
THUMBNAIL_DEBUG = DEBUG

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql', # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': 'sardaukar-91',                      # Or path to database file if using sqlite3.
        'USER': 'sardaukar-91',                      # Not used with sqlite3.
        'PASSWORD': 'mysisterbro14',                  # Not used with sqlite3.
        'HOST': '127.0.0.1',                      # Set to empty string for localhost. Not used with sqlite3.
        'PORT': '',                      # Set to empty string for default. Not used with sqlite3.
    }
}