/**
 * Created with PyCharm.
 * User: limeschnaps
 * Date: 23.10.13
 * Time: 2:08
 * To change this template use File | Settings | File Templates.
 */

String.prototype.format = function() {
    var s = this.toString();
    for (var i=0; i<arguments.length; i++) {
        s = s.replace('{'+i+'}', arguments[i]);
    }
    return s;
};

$('.ui.dropdown').dropdown();
$('.ui.accordion').accordion();
//$('.ui.carousel').carousel();

$('.ui.moneo-corner-label').popup();

$('.moneo-list .item *:not(".action-add"):not(".action-add-form")').on('click', function(e) {
    if ($(this).parents('.item').data('href'))
       window.location = $(this).parents('.item').data('href');
});

var updateCartSizeGlobal = function(newSize) {
    $('.ui.cart-size').text(newSize);
}

$('.action-add-form').on('submit', function(e) {
    e.preventDefault();
    $form = $(this);
    $url = $form.attr('action');
    $.ajax({
        url: $url,
        type: 'POST',
        data: $form.serialize(),
        beforeSend: function() {
            $form.find('button').addClass('loading');
        },
        success: function(response) {
            updateCartSizeGlobal(response.size);
            $form
                .find('button')
                .removeClass('loading')

            $('.ui.message.added-to-cart').fadeIn().delay(3000).fadeOut();
        },
        error: function(a,b,c) {
            console.error(a,b,c);
        }
    })
})

$('.action-remove-form').on('submit', function(e) {
    e.preventDefault();
    $form = $(this);
    $url = $form.attr('action');
    $.ajax({
        url: $url,
        type: 'POST',
        data: $form.serialize(),
        beforeSend: function() {
            $form.find('button').addClass('loading');
        },
        success: function(response) {
            updateCartSizeGlobal(response.size);
            $form.find('button').removeClass('loading');
            var itemRow = $($form.parents('tr.item')[0]);
            itemRow.fadeOut(500, function() {
                var tbody = $($form.parents('tbody')[0]);
                $(this).remove();
                console.log(response.sum_delta)
                $('.cart-sum').text(parseInt($('.cart-sum').text()) - response.sum_delta);
                if (tbody.find('.item').length == 0) {
                    tbody.prepend('<tr><td colspan="6" style="text-align: center; padding: 30px 0; color: #888888;">Корзина пуста</td></tr>');
                    $('.page').find('.submit').addClass('disabled');
                }
            });
        },
        error: function(a,b,c) {
            console.error(a,b,c);
        }
    })
});

$('.action-submit-form').on('submit', function(e) {
    e.preventDefault();
    $form = $(this);
    $url = $form.attr('action');
    $.ajax({
        url: $url,
        type: 'POST',
        data: $form.serialize(),
        beforeSend: function() {
            $form.parents('.form').addClass('loading');
            $form.find('.error').hide();
        },
        success: function(response) {
            $form.parents('.form').removeClass('loading');
            window.location = '/cart/payment/';
        },
        error: function(response, type, error) {
            var t = setTimeout(function() {
                console.error(response.responseJSON.errors);
                $.each(response.responseJSON.errors, function(field, error) {
                    if (field == 'captcha') {
                        field = field+'_1';
                        if (error[0].toUpperCase() == 'THE CODE YOU ENTERED IS WRONG.')
                            error = 'Неверный код.';
                    }
                    $form.find('input[name="'+field+'"]').parents('.field').find('.error').text(error).show();
                });
                $form.parents('.form').delay(2000).removeClass('loading');
                captchaSrc = $('.captcha img').attr('src').split('?')[0]+'{0}';
                $('.captcha img').attr('src', captchaSrc.format('?'+Math.random()));
            }, 1000);
        }
    });
});

$('.ui.button.submit').on('click', function(e) {
    if (!$(this).hasClass('disabled')) {
        window.location = $(this).data('href');
    }
});

$('.special').on('click', function() {
    window.location = $(this).data('href');
});