# -*- coding: utf-8 -*-

from hashlib import md5
from django.db import models
from tinymce.models import HTMLField
import json

class ShopCategory(models.Model):
    """
    Категория товаров
    """
    title = models.CharField(max_length=100, verbose_name=u'Название')
    order = models.PositiveSmallIntegerField(verbose_name=u'Порядок отображения в меню')
    slug = models.SlugField(verbose_name=u'Слаг категории для URL')
    is_available = models.BooleanField(verbose_name=u'Показывать', blank=True)
    parent = models.ForeignKey('self', verbose_name=u'Родительская категория', null=True, blank=True, limit_choices_to={'parent': None})

    def __unicode__(self):
        return u'%s' % self.title if self.parent is None else u'%s (%s)' % (self.title, self.parent)

    def display_name(self):
        return u'%s' % self.title

    def is_subcategory(self):
        return self.parent is not None

    def has_subcategories(self):
        return self.shopcategory_set.count() is not 0

    class Meta:
        verbose_name_plural = u'Категории товаров'
        verbose_name = u'Категория товара'
        ordering = ['order']


class Genre(models.Model):
    title = models.CharField(max_length=100, verbose_name=u'Название')
    order = models.PositiveSmallIntegerField(verbose_name=u'Порядок отображения в меню')
    slug = models.SlugField(verbose_name=u'Слаг категории для URL')

    def __unicode__(self):
        return u'%s' % self.title

    def display_name(self):
        return u'%s' % self.title

    class Meta:
        verbose_name_plural = u'Жанры игр'
        verbose_name = u'Жанр игры'


class GameShopItem(models.Model):
    """
    Игра - отображается в магазине
    """
    #GENRE_CHOICES = (
    #    ('shooter', u'Шутеры'),
    #    ('rpg', u'RPG'),
    #    ('mmorpg', u'MMORPG'),
    #    ('rts', u'Стратегии'),
    #    ('simulator', u'Симуляторы'),
    #    ('indie', u'Инди')
    #)

    title = models.CharField(max_length=100, verbose_name=u'Название')
    image = models.ImageField(upload_to='games', verbose_name=u'Изображение')
    price = models.IntegerField(verbose_name=u'Цена')
    description = HTMLField()
    instructions = HTMLField()
    category = models.ForeignKey(ShopCategory, verbose_name=u'Категория товара')
    genre = models.ForeignKey(Genre, verbose_name=u'Жанр', blank=True, null=True)
    publisher = models.CharField(max_length=100, verbose_name=u'Издатель/Разработчик', blank=True, null=True,
                                 help_text=u'Например: Valve'
                                           u'Указывать что-либо одно - либо оригинального издателя'
                                           u'Либо оригинального разработчика')
    publisher_ru = models.CharField(max_length=100, verbose_name=u'Издатель в РФ', blank=True, null=True)
    linked_items = models.ManyToManyField('self', verbose_name=u'Связанные товары', blank=True, null=True, limit_choices_to={'is_available': True})
    is_available = models.BooleanField(verbose_name=u'Показывать', blank=True, default=True)
    is_new = models.BooleanField(verbose_name=u'Новый товар?', blank=True, default=True)

    crop_options = models.CharField(
        max_length=100,
        verbose_name=u'Опции обрезки изображения',
        default=u'center',
        help_text=u'Возможные варианты: left right top bottom center Npx'
    )

    resize_options = models.CharField(
        max_length=100,
        verbose_name=u'Опции размера изображения',
        default=u'1140x350',
        help_text=u'Размер изображения в виде WxH, где W - ширина, H - высота. Например 1140x350'
    )

    def __unicode__(self):
        return u'%s (%s)' % (self.title, self.category)

    def is_in_stock(self):
        return True if self.gamekeyshopitem_set.count() else False

    def get_linked_items(self):
        return self.linked_items.filter(is_available=True)

    def has_metadata(self):
        if self.genre or self.publisher or self.publisher_ru:
            return True

        return False

    class Meta:
        verbose_name_plural = u'Товары'
        verbose_name = u'Товар'


class GameKeyShopItem(models.Model):
    """
    Ключ для игры - не отображается в магазине
    """
    for_game = models.ForeignKey(GameShopItem)
    key = models.CharField(max_length=100, verbose_name=u'Ключ игры', default=u'No code supplied, use image', null=True, blank=True)
    key_image = models.ImageField(upload_to='game_keys', verbose_name=u'Изображение ключа игры', null=True, blank=True)

    def __unicode__(self):
        return '%s (%s)' % (self.key, self.for_game)

    class Meta:
        verbose_name_plural = u'Коды для игр'
        verbose_name = u'Код для игры'


class GoldShopItem(models.Model):
    SERVER_CHOICES = (
        ('GOR', 'Гордунни'),
        ('BS', 'Черный шрам')
    )

    price = models.IntegerField(verbose_name=u'Цена за 1000')
    price_gradation = models.CharField(max_length=300, verbose_name=u'Градация цен в зависимости от количества')

    def __unicode__(self):
        return u'Золото'

    def get_gradation(self):
        obj = []
        gradation_string = self.price_gradation
        gradation = gradation_string.split(';')
        for item in gradation:
            g = item.split(':')
            obj.append({'from': int(g[0]), 'to': int(g[1]), 'off': float(g[2])})

        # return json.dumps(obj)
        return obj

    class Meta:
        verbose_name = u'Золото'
        verbose_name_plural = u'Золото'


class Order(models.Model):
    """
    Заказ
    """
    invoice = models.CharField(max_length=32, verbose_name=u'ID заказа')
    created = models.DateTimeField(verbose_name=u'Создан')
    email = models.EmailField(verbose_name=u'Адрес эл. почты')
    total = models.PositiveIntegerField(verbose_name=u'Сумма заказа')

    def __unicode__(self):
        return u'Заказ №%s (%s) на сумму %d [%s]' % (self.pk, self.email, self.total, self.created)

    class Meta:
        verbose_name_plural = u'Заказы'
        verbose_name = u'Заказ'


class OrderItem(models.Model):
    """
    Позиция заказа
    """
    order = models.ForeignKey(Order, verbose_name=u'Заказ')
    item = models.ForeignKey(GameShopItem, verbose_name=u'Игра')
    count = models.PositiveIntegerField(verbose_name=u'Количество')

    def __unicode__(self):
        return u'%s (%d)' % (self.item, self.count)

    class Meta:
        verbose_name_plural = u'Позиции заказов'
        verbose_name = u'Позиция заказа'


class GoldOrderItem(models.Model):
    """
    Позиция заказа
    """
    order = models.ForeignKey(Order, verbose_name=u'Заказ')
    #item = models.ForeignKey(GameShopItem, verbose_name=u'Игра')
    price = models.PositiveIntegerField(verbose_name=u'Количество')

    def __unicode__(self):
        return u'%s (%d)' % (self.item, self.count)

    class Meta:
        verbose_name_plural = u'Позиции заказов'
        verbose_name = u'Позиция заказа'


class SpecialItem(models.Model):
    """
    Блок спецакции для главной
    """
    title = models.CharField(max_length=100, verbose_name=u'Название')
    message = models.TextField(verbose_name=u'Текст блока')
    image = models.ImageField(upload_to='specials', verbose_name=u'Изображение')
    url = models.URLField(verbose_name=u'URL на объект', null=True, blank=True)
    item = models.ForeignKey(GameShopItem, verbose_name=u'Товар')
    is_available = models.BooleanField(verbose_name=u'Показывать', blank=True)

    crop_options = models.CharField(
        max_length=100,
        verbose_name=u'Опции обрезки изображения',
        default=u'center',
        help_text=u'Возможные варианты: left right top bottom center Npx'
    )

    resize_options = models.CharField(
        max_length=100,
        verbose_name=u'Опции размера изображения',
        default=u'1140x350',
        help_text=u'Размер изображения в виде WxH, где W - ширина, H - высота. Например 1140x350'
    )

    def __unicode__(self):
        return self.title


class FAQItem(models.Model):
    question = models.CharField(max_length=200, verbose_name=u'Вопрос')
    answer = HTMLField(verbose_name=u'Ответ')
    order = models.PositiveSmallIntegerField(verbose_name=u'Порядок отображения в меню')

    def __unicode__(self):
        return u'Вопрос: %s' % self.question

    class Meta:
        verbose_name = u'Блок вопрос-ответ'
        verbose_name_plural = u'Блоки вопрос-ответ'
        ordering = ['order']


class FeedbackItem(models.Model):
    added = models.DateTimeField(auto_now_add=True, verbose_name=u'Дата добавления', editable=True)
    author = models.CharField(max_length=50, verbose_name=u'Автор')
    message = models.TextField(verbose_name=u'Текст отзыва')
    is_published = models.BooleanField(verbose_name=u'Опубликован')

    def get_gravatar_url(self):
        return 'http://gravatar.com/avatar/%s/?s=80&default=identicon' % md5(self.author).hexdigest()