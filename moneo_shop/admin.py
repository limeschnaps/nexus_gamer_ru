# -*- coding: utf-8 -*-

__author__ = 'limeschnaps'

from django.contrib import admin
from .models import (Order,
                     OrderItem,
                     GameShopItem,
                     GameKeyShopItem,
                     SpecialItem,
                     ShopCategory,
                     GoldShopItem,
                     Genre,
                     FeedbackItem)


# Inlines
class OrderItemInline(admin.TabularInline):
    model = OrderItem
    extra = 0


class GameKeyShopItemInline(admin.TabularInline):
    model = GameKeyShopItem
    extra = 0


class OrderAdmin(admin.ModelAdmin):
    inlines = [OrderItemInline,]


class GameShopItemAdmin(admin.ModelAdmin):
    inlines = [GameKeyShopItemInline,]


admin.site.register(GameShopItem, GameShopItemAdmin)
admin.site.register(GameKeyShopItem)
admin.site.register(Order, OrderAdmin)
admin.site.register(OrderItem)
admin.site.register(SpecialItem)
admin.site.register(ShopCategory)
admin.site.register(GoldShopItem)
admin.site.register(Genre)
admin.site.register(FeedbackItem)
