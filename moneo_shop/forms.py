# -*- coding: utf-8 -*-

__author__ = 'limeschnaps'


from django import forms
from supercaptcha import CaptchaField, CaptchaWidget
from .models import FeedbackItem


class CartSubmitForm(forms.Form):
    email = forms.EmailField(label='E-mail', required=True)
    email_c = forms.EmailField(label='Подтвердите E-mail', required=True)
    captcha = CaptchaField(label=u'Введите код', widget=CaptchaWidget(attrs={'class': 'form-code', 'tabindex': '4'}))

    # def is_valid(self):
    #     self._errors = {}
    #     if self.data.get('email') != self.data.get('email_c'):
    #         print 'Eror'
    #         self._errors.update({'email_c': u'Адреса не совпадают'})
    #
    #     if self._errors:
    #         return False
    #
    #     return True

    def clean(self):
        cleaned_data = super(CartSubmitForm, self).clean()
        email = cleaned_data.get('email')
        email_c = cleaned_data.get('email_c')

        if email != email_c:
            msg = u'Адреса не совпадают.'
            self._errors['email'] = self.error_class([msg])
            self._errors['email_c'] = self.error_class([msg])

            del cleaned_data['email']
            del cleaned_data['email_c']


class FeedbackForm(forms.ModelForm):
    class Meta:
        model = FeedbackItem
        fields = ['author', 'message']