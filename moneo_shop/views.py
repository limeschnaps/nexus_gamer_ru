# -*- coding: utf-8 -*-

__author__ = 'limeschnaps'

import json
import datetime
from django.conf import settings
from django.views.generic import View, TemplateView, ListView, DetailView, FormView
from django.http import Http404, HttpResponse, HttpResponseRedirect
from django.db.models import Q
from hashlib import md5
from .models import ShopCategory, SpecialItem, GameShopItem, FAQItem, FeedbackItem, Order, OrderItem, GoldShopItem, Genre
from .forms import CartSubmitForm, FeedbackForm
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from .lib.paginator import DiggPaginator

class MoneoMixin(View):
    active_page = None
    active_step = None

    def get_context_data(self, **kwargs):
        context = super(MoneoMixin, self).get_context_data(**kwargs)
        menu_items = ShopCategory.objects.filter(is_available=True, parent=None).order_by('order')
        genre_items = Genre.objects.all()
        try:
            self.request.session['cart']
        except:
            self.request.session['cart'] = {}
        context['menu_items'] = menu_items
        context['active_page'] = self.active_page
        context['active_step'] = self.active_step
        context['size'] = len(self.request.session.get('cart', {}))
        context['genres'] = genre_items
        return context

    def generate_cart(self):
        cart = {
            'items': [],
            'sum': 0
        }

        session_obj = self.request.session.get('cart')
        for k, v in session_obj.items():
            try:
                item_obj = GameShopItem.objects.get(pk=k)
            except:
                pass

            item = {
                'id': item_obj.id,
                'title': item_obj.title,
                'price': item_obj.price,
                'quantity': v,
                'sum': item_obj.price * v
            }

            cart['items'].append(item)
            cart['sum'] += item_obj.price * v

        return cart


class HomePageView(MoneoMixin, TemplateView):
    template_name = 'index.html'
    active_page = 'index'

    def get_context_data(self, **kwargs):
        context = super(HomePageView, self).get_context_data(**kwargs)
        special_items = SpecialItem.objects.filter(is_available=True)

        # TODO: Filter items with flag `is_popular`
        context['popular_items'] = GameShopItem.objects.filter(is_available=True)

        context['special_items'] = special_items
        return context


class CategoryPageView(MoneoMixin, ListView):
    template_name = 'category.html'
    context_object_name = 'shop_items'

    def update_context(self):
        context = super(CategoryPageView, self).get_context_data(**kwargs)
        context['abc'] = 1000

    def get_context_data(self, *args, **kwargs):
        context = super(CategoryPageView, self).get_context_data(**kwargs)
        category = self.kwargs.get('category', None)
        subcategory = self.kwargs.get('subcategory', None)
        context['category'] = ShopCategory.objects.get(slug=category) if category else None
        context['subcategory'] = ShopCategory.objects.get(parent__slug=category, slug=subcategory) if subcategory else None
        return context

    def get_queryset(self, *args, **kwargs):
        category = self.kwargs.get('category', None)
        subcategory = self.kwargs.get('subcategory', None)
        self.active_page = category
        if subcategory:
            queryset = GameShopItem.objects.filter(category__parent__slug=category, category__slug=subcategory)
        else:
            queryset = GameShopItem.objects.filter(category__slug=category, category__parent__isnull=True)

        page = self.request.GET.get('page', 1)
        # paginator = DiggPaginator(queryset, 1)
        paginator = DiggPaginator(queryset, 16)

        try:
            queryset = paginator.page(page)
        except EmptyPage:
            queryset = paginator.page(paginator.num_pages)

        return queryset


class GenrePageView(MoneoMixin, ListView):
    template_name = 'category.html'
    context_object_name = 'shop_items'

    def get_context_data(self, *args, **kwargs):
        context = super(GenrePageView, self).get_context_data(*args, **kwargs)
        context['category'] = Genre.objects.get(slug=self.kwargs.get('genre', None))
        return context

    def get_queryset(self, *args, **kwargs):
        genre = self.kwargs.get('genre', None)
        self.active_page = genre

        if genre:
            queryset = GameShopItem.objects.filter(genre__slug=genre)
        else:
            raise Http404

        page = self.request.GET.get('page', 1)
        paginator = DiggPaginator(queryset, 16)

        try:
            queryset = paginator.page(page)
        except EmptyPage:
            queryset = paginator.page(paginator.num_pages)

        return queryset

class ItemDetailPageView(MoneoMixin, DetailView):
    template_name = 'detail.html'
    context_object_name = 'shop_item'

    def get(self, request, *args, **kwargs):
        try:
            self.queryset = GameShopItem.objects.filter(pk=kwargs.get('pk'))
            self.active_page = self.queryset[0].category.parent.slug if self.queryset[0].category.is_subcategory() else self.queryset[0].category.slug
        except:
            raise Http404

        return super(ItemDetailPageView, self).get(request, *args, **kwargs)


class FAQPageView(MoneoMixin, ListView):
    template_name = 'faq.html'
    active_page = 'faq'
    context_object_name = 'faq_items'
    queryset = FAQItem.objects.all()


class FeedbackPageView(MoneoMixin, FormView):
    template_name = 'feedback.html'
    active_page = 'feedback'
    #context_object_name = 'feedback_items'
    form_class = FeedbackForm
    #queryset = FeedbackItem.objects.filter(is_published=True)

    def get_context_data(self, *args, **kwargs):
        context = super(FeedbackPageView, self).get_context_data(*args, **kwargs)
        context['feedback_items'] = FeedbackItem.objects.filter(is_published=True)
        return context

    def form_valid(self, form):
        print 'VALID'
        print form.cleaned_data
        feedback_item = FeedbackItem()
        feedback_item.author = form.cleaned_data.get('author')
        feedback_item.message = form.cleaned_data.get('message')
        feedback_item.is_published = False
        feedback_item.save()
        return super(FeedbackPageView, self).get(self.request)

    #def post(self, request, *args, **kwargs):
    #    print 'POSTED!'
    #    feedback_item = FeedbackItem()
    #
    #    return super(FeedbackPageView, self).post(request, *args, **kwargs)


class SearchPageView(MoneoMixin, ListView):
    template_name = 'search.html'
    active_page = 'search'
    context_object_name = 'results'

    def get_queryset(self):
        term = self.request.GET.get('q', None)

        if term:
            f = Q(title__icontains=term) | Q(description__icontains=term)
            return GameShopItem.objects.filter(f)
        else:
            return GameShopItem.objects.all()

class GoldTradePageView(MoneoMixin, TemplateView):
    template_name = 'gold.html'
    active_page = 'wow'

    def get_context_data(self, **kwargs):
        context = super(GoldTradePageView, self).get_context_data(**kwargs)
        context['gold_item'] = GoldShopItem.objects.get(pk=1)
        return context

    def get(self, request, *args, **kwargs):
        amount = self.request.GET.get('amount')
        convert_type = self.request.GET.get('type')

        print '\n'*10, amount, convert_type, '\n'*10

        if amount and convert_type:
            try:
                amount = int(amount)
            except:
                pass

            if amount and (convert_type == 'gold' or convert_type == 'cash'):
                # off = 0
                gold = self.get_context_data()['gold_item']

                gradations = gold.get_gradation()
                for g in gradations:
                    if amount in range(g.get('from'), g.get('to')+1):
                        off = g.get('off')
                        break

                price = gold.price - (gold.price * off / 100)
                print off, price

                value = int(amount / 1000) * price

                return HttpResponse(json.dumps({'value': value}), mimetype='application/json', status=200)

        return super(GoldTradePageView, self).get(request, *args, **kwargs)


class CartPageView(MoneoMixin, TemplateView):
    template_name = 'cart_view.html'
    active_page = 'cart'

    def get_context_data(self, **kwargs):
        context = super(CartPageView, self).get_context_data(**kwargs)
        context['cart'] = self.generate_cart()
        return context


class CartSubmitPageView(MoneoMixin, FormView):
    template_name = 'cart_submit.html'
    active_page = 'cart'
    active_step = 1
    form_class = CartSubmitForm

    def form_valid(self, form):
        cart = self.generate_cart()

        order = Order()
        order.email = form.data['email']
        order.total = cart.get('sum')
        order.created = datetime.datetime.utcnow()
        order.invoice = md5(order.created.strftime('%d%m%y%H%M%S') + order.email + str(order.total)).hexdigest()
        order.save()

        for item in cart.get('items'):
            order_item = OrderItem()
            order_item.order = order
            order_item.item_id = item.get('id')
            order_item.count = item.get('quantity')
            order_item.save()

        self.request.session['order'] = order
        self.request.session.save()
        return HttpResponse(json.dumps({'status': 200}), mimetype='application/json', status=200)

    def form_invalid(self, form):
        return HttpResponse(json.dumps({'errors': form.errors}), mimetype='application/json', status=400)

    def get(self, request, *args, **kwargs):
        if len(request.session.get('cart')) == 0:
            return HttpResponseRedirect('/cart')
        else:
            return super(CartSubmitPageView, self).get(request, *args, **kwargs)


class CartPaymentPageView(MoneoMixin, TemplateView):
    template_name = 'cart_payment.html'
    active_page = 'cart'
    active_step = 2

    def get_context_data(self, **kwargs):
        context = super(CartPaymentPageView, self).get_context_data(**kwargs)
        order = self.request.session.get('order', None)
        if not order:
            return context

        context['order'] = order

        robokassa_crc_string = '%s:%s:%s:%s' % (settings.ROBOKASSA_MERCHANT_LOGIN, order.total, order.id, settings.ROBOKASSA_MERCHANT_PASS_1)
        robokassa_crc = md5(robokassa_crc_string).hexdigest()

        description = 'Заказ в магазине %s' % settings.ROBOKASSA_SHOP_NAME

        robokassa_pay = 'https://auth.robokassa.ru/Merchant/Index.aspx?' \
                           'MerchantLogin=%s&' \
                           'OutSum=%s&' \
                           'InvoiceID=%s&' \
                           'SignatureValue=%s&' \
                           'Description=%s&' \
                           'Culture=%s&' \
                           'Encoding=%s' % (
                                settings.ROBOKASSA_MERCHANT_LOGIN,
                                order.total,
                                order.id,
                                robokassa_crc,
                                description,
                                settings.ROBOKASSA_CULTURE,
                                settings.ROBOKASSA_ENCODING
                           )
        context['robokassa'] = robokassa_pay
        return context


class CartAddAction(View):
    def post(self, request, *args, **kwargs):
        goods_id = request.POST.get('gid', None)
        if not goods_id:
            return HttpResponse(json.dumps({'status': 400}), mimetype='application/json', status=400)

        try:
            request.session['cart']
        except:
            request.session['cart'] = {}

        request.session['cart'].update({goods_id: 1})
        request.session.save()

        return HttpResponse(json.dumps({'status': 200, 'size': len(request.session['cart'])}), mimetype='application/json', status=200)


class CartRemoveAction(View):
    def post(self, request, *args, **kwargs):
        goods_id = request.POST.get('gid', None)
        if not goods_id:
            return HttpResponse(json.dumps({'status': 400}), mimetype='application/json', status=400)

        try:
            request.session['cart']
        except:
            request.session['cart'] = {}

        try:
            quantity = request.session['cart'].pop(goods_id)
            sum_delta = GameShopItem.objects.get(pk=goods_id).price * quantity
            request.session.save()
        except:
            sum_delta = 0

        return HttpResponse(json.dumps({'status': 200, 'size': len(request.session['cart']), 'sum_delta': sum_delta}), mimetype='application/json', status=200)


class CartPaymentCheckAction(MoneoMixin, TemplateView):
    template_name = 'index.html'

    def get(self, request, *args, **kwargs):
        order_total = request.GET.get('OutSum')
        order_id = request.GET.get('InvId')
        order_crc = request.GET.get('SignatureValue').upper()

        order = Order.objects.get(pk=order_id)
        order_total = order.total

        local_order_crc = md5('%s:%s:%s' % (order_total, order_id, settings.ROBOKASSA_MERCHANT_PASS_2)).hexdigest().upper()

        if order_crc == local_order_crc:
            return HttpResponse('OK%s' % order_id)
        else:
            return HttpResponse('FAIL%s' % order_id)


home = HomePageView.as_view()
category = CategoryPageView.as_view()
genre = GenrePageView.as_view()
details = ItemDetailPageView.as_view()
faq = FAQPageView.as_view()
feedback = FeedbackPageView.as_view()
search = SearchPageView.as_view()
gold_trade = GoldTradePageView.as_view()
cart = CartPageView.as_view()
cart_submit = CartSubmitPageView.as_view()
cart_payment = CartPaymentPageView.as_view()

cart_add_action = CartAddAction.as_view()
cart_remove_action = CartRemoveAction.as_view()
cart_check_payment_action = CartPaymentCheckAction.as_view()